import React, { Component } from "react";
import formatText from "./api/formatText";
import "./styles/app.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textToFormat: "",
      completedText: "",
    };
  }

  onUpdateText = (e) => {
    let completedText = formatText(e.target.value);
    this.setState({
      completedText,
      textToFormat: e.target.value,
    });
  };

  render() {
    return (
      <div id="App">
        <div className="textContainer">
          <h1>Hi there!</h1>
          <p>Type a message in the box below to tranlsate to sArCaStIc TeXt!</p>
        </div>

        <div className="inputContainer">
          <input
            type="text"
            placeholder="Enter text to be translated"
            className="beforeTranslationInput"
            onChange={this.onUpdateText}
          ></input>
        </div>

        <p id="textToFormat">{this.state.completedText}</p>
      </div>
    );
  }
}

export default App;
