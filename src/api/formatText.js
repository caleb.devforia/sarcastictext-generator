function formatText(text) {
  if (!text || text === "") return false;
  let regex = / /g;
  let completeText = text.split(regex);
  let lengths = completeText.map((word) => word.length);
  let test = completeText.join("");
  let formattedText = "";
  let completeTextArr = [];

  for (let i = 0; i < test.length; i++) {
    formattedText +=
      i % 2 === 1 ? test.charAt(i).toUpperCase() : test.charAt(i);
  }

  lengths.map((length, index) => {
    let startIndex = test.indexOf(completeText[index]);
    let endIndex = startIndex + length;

    completeTextArr.push(formattedText.substring(startIndex, endIndex));
    // completeText.splice(index, 0, formattedText);
  });

  return completeTextArr.join(" ");
}

export default formatText;
